/* global Drupal, jQuery, phpdebugbar */

Drupal.behaviors.debugbar = {
  /**
   * @param e
   * @param xhr
   */
  handleError: function (e, xhr) {
    /** @var phpdebugbar.ajaxHandler AjaxHandler */
    phpdebugbar.ajaxHandler.handle(xhr);
  },
  attach: function () {
    /*
    debugbar uses ajaxComplete() but Drupal core already binds on that
    and throws an error if the AJAX request fails,
    so debugbar never gets the event.
    To work around that, we also bind on ajaxError().
     */
    jQuery(document).once().ajaxError(this.handleError);
  }
};
