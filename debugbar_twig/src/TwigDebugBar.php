<?php

namespace Drupal\debugbar_twig;

use DebugBar\Bridge\NamespacedTwigProfileCollector;
use DebugBar\DataCollector\DataCollectorInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\debugbar\DrupalDebugBar;
use Symfony\Component\HttpFoundation\RequestStack;
use Twig\Profiler\Profile;

/**
 * Extends DrupalDebugBar to add a Twig profiler.
 *
 * @package Drupal\debugbar_twig
 */
class TwigDebugBar extends DrupalDebugBar {

  /**
   * {@inheritDoc}
   */
  public function __construct(
    DataCollectorInterface $logger,
    RouteMatchInterface $routeMatch,
    RequestStack $requestStack,
    Profile $profile
  ) {
    parent::__construct($logger, $routeMatch, $requestStack);

    $this->addCollector(new NamespacedTwigProfileCollector($profile));
  }

}
