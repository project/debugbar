# PHP Debug Bar

This module uses [PHP Debug Bar](http://phpdebugbar.com/) to display various information
about the current request :

- Drupal log messages
- POST and GET variables
- Site settings
- Route name and parameters
- Exceptions

**This module should not be enabled in a production environment.**

## Requirements

This module requires the following modules:

- [Vendor Stream Wrapper](https://www.drupal.org/project/vendor_stream_wrapper)

And the following Composer packages:

- [maximebf/debugbar](https://packagist.org/packages/maximebf/debugbar)

## Installation

Install as you would normally install a contributed Drupal module.
For further information,
see [Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).

Installation with Composer is recommended,
so that it installs the debugbar library automatically.

## Configuration

There is no configuration.
Just enable the module and it will start displaying the debug bar.

The debugbar_twig submodule adds a new Twig pane to the debug bar.

## Maintainers

- [Insite](https://www.drupal.org/insite)
