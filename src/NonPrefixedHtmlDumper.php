<?php

namespace Drupal\debugbar;

use DebugBar\DataFormatter\VarDumper\DebugBarHtmlDumper;

/**
 * DebugBarHtmlDumper replacement without prefixed CSS selectors.
 *
 * @package Drupal\debugbar
 */
class NonPrefixedHtmlDumper extends DebugBarHtmlDumper {

  /**
   * Dumps the HTML header.
   *
   * @return string
   *   HTML header.
   */
  public function getDumpHeaderByDebugBar(): string {
    /*
     * DebugBarHtmlDumper prefixes some CSS selectors
     * but this breaks the devel var-dumper,
     * so we don't want to do that.
     */
    return $this->getDumpHeader();
  }

}
