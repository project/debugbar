<?php

namespace Drupal\debugbar\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Font Awesome fonts controller.
 *
 * We need this controller
 * because the relative font path in font-awesome.min.css
 * does not work with vendor_stream_wrapper.
 *
 * @package Drupal\debugbar\Controller
 */
class FontsController extends ControllerBase {

  /**
   * Redirect to the real font path.
   *
   * @param string $filename
   *   Font filename.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   Response.
   */
  public function getFont(string $filename): RedirectResponse {
    return new RedirectResponse(
      vendor_stream_wrapper_create_url(
        'vendor://maximebf/debugbar/src/DebugBar/Resources/vendor/font-awesome/fonts/' . $filename
      )
    );
  }

}
