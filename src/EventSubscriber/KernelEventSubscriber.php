<?php

namespace Drupal\debugbar\EventSubscriber;

use DebugBar\DebugBar;
use Drupal\Core\Routing\RouteMatchInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;
use Symfony\Component\HttpKernel\Event\ResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * Event subscriber used to catch AJAX and redirect responses.
 *
 * @package Drupal\debugbar\EventSubscriber
 */
class KernelEventSubscriber implements EventSubscriberInterface {

  /**
   * Debug bar.
   *
   * @var \DebugBar\DebugBar
   */
  private $debugBar;

  /**
   * Route match.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  private $routeMatch;

  /**
   * KernelEventSubscriber constructor.
   *
   * @param \DebugBar\DebugBar $debugBar
   *   Debug bar.
   * @param \Drupal\Core\Routing\RouteMatchInterface $routeMatch
   *   Route match.
   */
  public function __construct(DebugBar $debugBar, RouteMatchInterface $routeMatch) {
    $this->debugBar = $debugBar;
    $this->routeMatch = $routeMatch;
  }

  /**
   * KernelEvents::EXCEPTION event handler.
   *
   * @param \Symfony\Component\HttpKernel\Event\ExceptionEvent $event
   *   Event.
   *
   * @throws \Exception
   */
  public function onException(ExceptionEvent $event) {
    /** @var \DebugBar\DataCollector\ExceptionsCollector $collector */
    $collector = $this->debugBar->getCollector('exceptions');

    $collector->addThrowable($event->getThrowable());
  }

  /**
   * KernelEvents::REQUEST event handler.
   *
   * @param \Symfony\Component\HttpKernel\Event\ResponseEvent $event
   *   Event.
   */
  public function onResponse(ResponseEvent $event) {
    if ($event->getRequest()->isXmlHttpRequest()) {
      $event->getResponse()->headers->add($this->debugBar->getDataAsHeaders('phpdebugbar', 4096, 128000));
    }
    elseif ($event->getResponse()->isRedirect()) {
      // Don't pollute collectors with vendor_stream_wrapper responses.
      if (!in_array(
        $this->routeMatch->getRouteName(), [
          'vendor_stream_wrapper.vendor_file_download',
          'debugbar.fonts',
        ])) {
        $this->debugBar->stackData();
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    return [
      KernelEvents::EXCEPTION => 'onException',
      KernelEvents::RESPONSE => 'onResponse',
    ];
  }

}
