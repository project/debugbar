<?php

namespace Drupal\debugbar;

use DebugBar\DataFormatter\DebugBarVarDumper;

/**
 * Extends DebugBarVarDumper to use our own dumper class.
 *
 * @package Drupal\debugbar
 */
class VarDumper extends DebugBarVarDumper {

  /**
   * {@inheritDoc}
   */
  protected function getDumper() {
    if (!$this->dumper) {
      // We want to use our own HTML dumper class.
      $this->dumper = new NonPrefixedHtmlDumper();

      $dumperOptions = $this->getDumperOptions();
      if (isset($dumperOptions['styles'])) {
        $this->dumper->setStyles($dumperOptions['styles']);
      }
    }

    return $this->dumper;
  }

}
