<?php

namespace Drupal\debugbar;

use Drupal\Core\Render\Markup;
use Drupal\Core\Security\TrustedCallbackInterface;

/**
 * Debug bar collecting information about the current request.
 *
 * @package Drupal\debugbar
 */
class LazyBuilder implements TrustedCallbackInterface {

  /**
   * {@inheritdoc}
   * @link https://www.drupal.org/node/2966725
   */
  public static function trustedCallbacks(): array {
    return ['renderDebugBar'];
  }

  /**
   * Render the debug bar.
   *
   * @return array
   *   Render array.
   */
  public static function renderDebugBar(): array {
    /** @var \DebugBar\DebugBar $debugBar */
    $debugBar = \Drupal::service('debugbar.debugbar');

    return [
      '#markup' => Markup::create(
        $debugBar->getJavascriptRenderer()->render()
      ),
    ];
  }

}
