<?php

namespace Drupal\debugbar;

use DebugBar\DataCollector\MessagesCollector;
use Drupal\Core\Logger\LogMessageParserInterface;
use Drupal\Core\Logger\RfcLogLevel;
use Psr\Log\LogLevel;

/**
 * Collector for Drupal log messages.
 *
 * We extend MessagesCollector to work around the fact that Drupal
 * does not use PSR-compliant log levels.
 *
 * @package Drupal\debugbar
 * @link https://www.drupal.org/project/drupal/issues/3062351
 */
class DebugBarLogger extends MessagesCollector {

  /**
   * Message parser.
   *
   * @var \Drupal\Core\Logger\LogMessageParserInterface
   */
  private $parser;

  /**
   * DebugBarLogger constructor.
   *
   * @param \Drupal\Core\Logger\LogMessageParserInterface $parser
   *   Message parser.
   */
  public function __construct(LogMessageParserInterface $parser) {
    parent::__construct();

    $this->parser = $parser;
  }

  /**
   * Map of RFC 5424 log constants to PSR3 log constants.
   *
   * @var array
   *
   * @see \Drupal\Core\Logger\LoggerChannel::$levelTranslation
   */
  private $levelTranslation = [
    RfcLogLevel::EMERGENCY => LogLevel::EMERGENCY,
    RfcLogLevel::ALERT => LogLevel::ALERT,
    RfcLogLevel::CRITICAL => LogLevel::CRITICAL,
    RfcLogLevel::ERROR => LogLevel::ERROR,
    RfcLogLevel::WARNING => LogLevel::WARNING,
    RfcLogLevel::NOTICE => LogLevel::NOTICE,
    RfcLogLevel::INFO => LogLevel::INFO,
    RfcLogLevel::DEBUG => LogLevel::DEBUG,
  ];

  /**
   * {@inheritDoc}
   */
  public function log($level, $message, array $context = []): void {
    if (isset($this->levelTranslation[$level])) {
      $level = $this->levelTranslation[$level];
    }

    parent::log(
      $level,
      // Drupal does not use PSR-3 placeholders.
      strtr($message, $this->parser->parseMessagePlaceholders($message, $context)),
      $context
    );
  }

}
