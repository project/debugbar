<?php

namespace Drupal\debugbar;

use DebugBar\DataCollector\ConfigCollector;
use DebugBar\DataCollector\DataCollector;
use DebugBar\DataCollector\DataCollectorInterface;
use DebugBar\DataCollector\ExceptionsCollector;
use DebugBar\DataCollector\MemoryCollector;
use DebugBar\DataCollector\PhpInfoCollector;
use DebugBar\DataCollector\RequestDataCollector;
use DebugBar\DebugBar;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Site\Settings;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Debug bar collecting information about the current request.
 *
 * @package Drupal\debugbar
 */
class DrupalDebugBar extends DebugBar {

  /**
   * DebugBar constructor.
   *
   * @param \DebugBar\DataCollector\DataCollectorInterface $logger
   *   Message collector.
   * @param \Drupal\Core\Routing\RouteMatchInterface $routeMatch
   *   Route match.
   * @param \Symfony\Component\HttpFoundation\RequestStack $requestStack
   *   Request stack.
   *
   * @throws \DebugBar\DebugBarException
   */
  public function __construct(DataCollectorInterface $logger, RouteMatchInterface $routeMatch, RequestStack $requestStack) {
    // We extend the default dumper.
    DataCollector::setDefaultVarDumper(new VarDumper());

    $requestCollector = new RequestDataCollector();
    $configCollector = new ConfigCollector(Settings::getAll());
    $routeCollector = new ConfigCollector([
      'name' => $routeMatch->getRouteName(),
      'parameters' => $routeMatch->getParameters()->all(),
    ], 'route');

    $this->addCollector(new PhpInfoCollector())
      ->addCollector(new MemoryCollector())
      ->addCollector($logger)
      ->addCollector($requestCollector)
      ->addCollector($configCollector)
      ->addCollector($routeCollector)
      ->addCollector(new ExceptionsCollector());

    /*
     * Use the Symfony var dumper.
     * But only non non-AJAX requests,
     * because it makes the headers really big.
     */
    if (!$requestStack->getCurrentRequest()->isXmlHttpRequest()) {
      foreach ($this->getCollectors() as $collector) {
        if (method_exists($collector, 'useHtmlVarDumper')) {
          $collector->useHtmlVarDumper();
        }
      }
    }

    $jsRenderer = $this->getJavascriptRenderer();

    // Don't switch automatically to AJAX requests.
    $jsRenderer->setAjaxHandlerAutoShow(FALSE);

    // Drupal already provides jQuery.
    $jsRenderer->disableVendor('jquery');
  }

}
